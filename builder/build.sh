#!/bin/bash

##########################################################################
#  This build script is supposed to be run from Jenkins to build and     #
#  deploy a Docker image from the builder folder to the GitLab registry. #
#  Currently, the Jenkins job is called 'lcg_build_docker_images'        #
##########################################################################

set +x # No debug prints
set -e # Fail on error

# Current date
TIMESTAMP=$( date "+%Y-%m-%d" )

# GitLab registry URL
REGISTRY="gitlab-registry.cern.ch"

# GitLab repository name
BASENAME="sft/docker"

# The directory where this script is located; Important for the correct path later on
BUILDER_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Check that all variables are set (from Jenkins)
echo "================================================================================"
echo
echo "Platform:          ${PLATFORM:?You need to set PLATFORM (non-empty)}"
echo "Backup timestamp:  ${TIMESTAMP:?You need to set TIMESTAMP (non-empty)}"
echo
echo "GitLab registry:   ${REGISTRY:?You need to set REGISTRY (non-empty)}"
echo "Image base name:   ${BASENAME:?You need to set BASENAME (non-empty)}"
echo
echo "Host name:         $( hostname )"
echo "Builder directory: ${BUILDER_DIR:?You need to set BUILDER_DIR (non-empty)}"
echo
echo "================================================================================"
echo 
echo "[IMPORTANT] Remember to give the necessary access rights to sftnight:"
echo "            Add 'sftnight' as role 'Developer' for 1 day (set expiration date to tomorrow)"
echo "            https://gitlab.cern.ch/sft/docker/project_members"
echo
echo "================================================================================"

### Note from the editor: On the current builder nodes, user sftnight is already logged into the GitLab repository.
###                       This information is shared in a file on the shared AFS home directory.
###                       Keep the following section in case we run this script on another builder node.
#echo
#echo "Registry user:     ${REGISTRY_USER:?You need to set REGISTRY_USER (non-empty)}"
#: "${REGISTRY_PASSWORD:?You need to set REGISTRY_PASSWORD non-empty}"
#echo "Registry password: okay"
#
## Login to the GitLab registry as the specified user (probably sftnight)
#echo; echo "[INFO] Login to the GitLab Docker registry as user '${REGISTRY_USER}' ..."
#echo "${REGISTRY_PASSWORD}" | docker login --username "${REGISTRY_USER}" --password-stdin ${REGISTRY}
#
#function registry_logout {
#    # Remove local credentials for Docker registry
#    echo; echo "[INFO] Remove local credentials for the GitLab Docker registry ..."
#    docker logout ${REGISTRY}
#}
#
## Call logout automatically when the script exits (even after an error happened)
#trap registry_logout EXIT

# Remove the local image before building the current version
echo; echo "[INFO] Removing the old image '${PLATFORM}:latest' from the system ..."
docker rmi --force ${REGISTRY}/${BASENAME}/${PLATFORM}:latest

# Build the platform in the main folder of sft/docker; important for the path definitions inside the Dockerfiles
echo; echo "[INFO] Building the new ${PLATFORM} Docker image ..."
cd ${BUILDER_DIR}/..
docker build --pull --no-cache --tag ${REGISTRY}/${BASENAME}/${PLATFORM}:latest --file builder/${PLATFORM}/Dockerfile .

# Test new image before publication
if test "x${PLATFORM}" = "xfedora31" ; then
   # New platforms do not have python2: this check needs to be improved
   source ${BUILDER_DIR}/verify.sh "no"
else
   source ${BUILDER_DIR}/verify.sh "yes"
fi
   
# Push image to GitLab Docker registry
docker push ${REGISTRY}/${BASENAME}/${PLATFORM}:latest

# Docker image versioning: Tag image with the current date as version and push it to the registry
docker tag ${REGISTRY}/${BASENAME}/${PLATFORM}:latest ${REGISTRY}/${BASENAME}/${PLATFORM}:${TIMESTAMP}
docker push ${REGISTRY}/${BASENAME}/${PLATFORM}:${TIMESTAMP}

# Remove new images and tags locally
echo; echo "[INFO] Removing new ${PLATFORM} images ..."
docker rmi --force ${REGISTRY}/${BASENAME}/${PLATFORM}:latest
docker rmi --force ${REGISTRY}/${BASENAME}/${PLATFORM}:${TIMESTAMP}
